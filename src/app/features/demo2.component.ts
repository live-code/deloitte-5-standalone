import { CommonModule, NgForOf } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, inject, OnInit, signal } from '@angular/core';

@Component({
  selector: 'app-demo2',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    CommonModule
  ],
  template: `
    <h1>Signal Todos</h1>
    
    <input type="text" (keydown.enter)="addTodo($event)">
    
    <li *ngFor="let todo of todos(); let i = index">
      <input type="checkbox" [checked]="todo.completed" (change)="toggleTodo(todo)">
      {{todo.title}}
      <button (click)="deleteTodo(todo)">delete</button>
    </li>
    
    <pre>{{todos() | json}}</pre>
  `,
})
export default class Demo2Component implements OnInit {
  todos = signal<Todo[]>([])
  http = inject(HttpClient)

  ngOnInit() {
    this.http.get<Todo[]>('http://localhost:3000/todos')
      .subscribe(res => {
        this.todos.set(res)
      })
  }

  addTodo(e: Event) {
    this.http.post<Todo>('http://localhost:3000/todos', {
      title: (e.currentTarget as HTMLInputElement).value,
      completed: false
    })
      .subscribe(res => {
        this.todos.update(todos => [...todos, res])
      })
  }

  deleteTodo(todo: Todo) {
    this.http.delete(`http://localhost:3000/todos/${todo.id}`)
      .subscribe(() => {
        this.todos.update(todos => {
          return todos.filter(t => t.id !== todo.id)
        })
      })
  }

  toggleTodo(todo: Todo) {
    this.http.patch<Todo>(`http://localhost:3000/todos/${todo.id}`, {
      ...todo,
      completed: !todo.completed
    })
      .subscribe(res => {
        const index = this.todos().findIndex(t => t.id === todo.id)
        this.todos.mutate(todos => todos[index] = res)
      })


  }


  /*toggleTodo(todo: Todo) {
    console.log(todo)
    this.todos.update(todos => {
      return todos.map(t => {
        if (t.id === todo.id) {
          return {...t, completed: !t.completed}
        }
        return t;
      })
    })
  }*/
}


interface Todo {
  id: number;
  title: string;
  completed: boolean;
}
