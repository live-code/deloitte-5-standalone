import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, computed, signal } from '@angular/core';

@Component({
  selector: 'app-demo1',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule],
  template: `
    <h1>Signal Demo</h1>
    <pre>{{counter()}}</pre>
    <button (click)="dec()">-</button>
    <button (click)="inc()">+</button>
    
    <button (click)="reset()" *ngIf="!isZero()">reset</button>
    
    
  `,
})
export default class Demo1Component {
  counter = signal(5);
  isZero = computed(() => this.counter() === 0)

  inc() {
    this.counter.update(c => c + 1)
  }
  dec() {
    this.counter.update(c => c - 1)
  }

  reset() {
    this.counter.set(0)
  }
}

