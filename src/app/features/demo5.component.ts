import { HttpClient } from '@angular/common/http';
import { Component, inject, signal, effect } from '@angular/core';
import { CommonModule } from '@angular/common';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import {
  catchError,
  debounceTime,
  delay,
  filter,
  forkJoin,
  interval,
  map,
  mergeAll,
  mergeMap, of, Subscription,
  switchMap,
  tap
} from 'rxjs';

@Component({
  selector: 'app-demo5',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  template: `
    <h1>Demo toObservable</h1>
    
    <input
      type="text" 
      (input)="onChangeText($event)"
    >

    {{temp$ | async}}
    
  `,
})
export  default class Demo5Component {
  city = signal('')
  http = inject(HttpClient);

  temp$ = toObservable(this.city)
      .pipe(
          debounceTime(1000),
          switchMap(
           text => this.http.get<Meteo>(`http://api.openweathermap.org/data/2.5/weather?q=${text}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
            .pipe(
              catchError(() => of(null))
            )
          ),
          map(meteo => meteo?.main.temp)
      )


  onChangeText(e: Event) {
    this.city.set((e.currentTarget as HTMLInputElement).value)
  }
}





export interface Meteo {
  coord: Coord;
  weather?: (WeatherEntity)[] | null;
  base: string;
  main: Main;
  visibility: number;
  wind: Wind;
  clouds: Clouds;
  dt: number;
  sys: Sys;
  timezone: number;
  id: number;
  name: string;
  cod: number;
}
export interface Coord {
  lon: number;
  lat: number;
}
export interface WeatherEntity {
  id: number;
  main: string;
  description: string;
  icon: string;
}
export interface Main {
  temp: number;
  feels_like: number;
  temp_min: number;
  temp_max: number;
  pressure: number;
  humidity: number;
}
export interface Wind {
  speed: number;
  deg: number;
}
export interface Clouds {
  all: number;
}
export interface Sys {
  type: number;
  id: number;
  country: string;
  sunrise: number;
  sunset: number;
}

